# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function
from django.shortcuts import render

# Working code view

from django.views.generic import View

import gspread
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.discovery import build

import json
from httplib2 import Http
import numpy as np
#import pandas as pd




def index(request):
	return render(request, "pages/home.html",{})

def infodoc(request):
	scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
	creds = ServiceAccountCredentials.from_json_keyfile_name('abc.json', scope)
	client = gspread.authorize(creds)
	sheet = client.open("Testing Form (Responses)").sheet1
	list_of_hashes = sheet.get_all_records()
	
	'''print(type(list_of_hashes))
	absd= sheet.get_all_values()
	print(type(absd))
	str1 = ''.join(map(str,absd))
	print(str1)
	print(absd)
	#print(json.loads(list_of_hashes))'''
	
	context = {
		'time'  : np.asarray(sheet.col_values(1)),
		'email'	: np.asarray(sheet.col_values(2)),
	}
	
	return render(request, "pages/infodoc.html",context)